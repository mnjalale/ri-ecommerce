# Mars Yetu

Mars Yetu is a NativeScript-build iOS and Android app built using TypeScript and Angular 6. It is a mobile client that can be used with Nop Commerce web applications.


## Screenshots

![picture](screen_shots/Home.png)
![picture](screen_shots/Products%20By%20Category.png)

![picture](screen_shots/Add%20To%20Cart.png)
![picture](screen_shots/Shopping%20Cart.png)


