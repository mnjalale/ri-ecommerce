import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { HomeComponent } from "./components/home/home.component";
import { ProductsComponent } from "./components/products/products.component";
import { ProductComponent } from "./components/product/product.component";
import { LoginComponent } from "./components/authentication/login/login.component";
import { RegisterComponent } from "./components/authentication/register/register.component";
import { CartComponent } from "./components/cart/cart.component";
import { CheckoutComponent } from "./components/checkout/checkout.component";

const routes: Routes = [
    { path: "", redirectTo: "/home", pathMatch: "full" },
    { path: "home", component: HomeComponent },
    { path: "products/:categoryId", component: ProductsComponent },
    { path: "product/:productId", component: ProductComponent },
    { path: "login", component: LoginComponent },
    { path: "register", component: RegisterComponent },
    { path: "cart", component: CartComponent },
    { path: "checkout", component: CheckoutComponent }
    // { path: "home", loadChildren: "~/app/components/home/home.module#HomeModule" },
    // { path: "browse", loadChildren: "~/app/components/browse/browse.module#BrowseModule" },
    // { path: "search", loadChildren: "~/app/components/search/search.module#SearchModule" },
    // { path: "featured", loadChildren: "~/app/components/featured/featured.module#FeaturedModule" },
    // { path: "settings", loadChildren: "~/app/components/settings/settings.module#SettingsModule" }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
