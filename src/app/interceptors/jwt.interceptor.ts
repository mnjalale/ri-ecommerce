import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Config } from "../configurations/Config";


@Injectable()
export class JWTInterceptor implements HttpInterceptor {

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const sevenSpikesUrls = [
            Config.apiUrl + 'api/orders',
            Config.apiUrl + 'api/customers/' + Config.customerId
        ];


        if (request && request.url && sevenSpikesUrls.indexOf(request.url) >= 0) {
            request = request.clone({
                setHeaders: {
                    "Authorization": "Bearer " + Config.sevenSpikesToken
                }
            });
        } else {
            if (Config.token && Config.token.trim().length > 0) {
                request = request.clone({
                    setHeaders: {
                        "Content-Type": "application/json",
                        NST: Config.NST,
                        DeviceId: Config.deviceId,
                        Token: Config.token
                    }
                });
            } else {
                request = request.clone({
                    setHeaders: {
                        "Content-Type": "application/json",
                        NST: Config.NST,
                        DeviceId: Config.deviceId,
                    }
                });
            }
        }



        return next.handle(request);
    }
}