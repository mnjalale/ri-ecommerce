import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Config } from '../configurations/Config';
import { Observable, throwError } from 'rxjs';
import { LoginResult } from '../models/authentication/LoginResult';
import { catchError, map } from 'rxjs/operators';
import { LoginModel } from '../models/authentication/LoginModel';
import { RegisterModel } from '../models/authentication/RegisterModel';
import { ServerResponse } from '../models/utils/ServerResponse';
import { ServiceBase } from './base/service-base.service';

@Injectable()
export class AuthenticationService extends ServiceBase {

  constructor(private http: HttpClient) {
    super();
  }

  get isAuthenticated(): boolean {
    return Config.token && Config.token.length > 0;
  }

  login(model: LoginModel): Observable<LoginResult> {
    let url = Config.apiUrl + "api/login";
    return this.http
      .post<LoginResult>(url, model)
      .pipe(
        map(response => {
          if (response.StatusCode == 200) {
            Config.token = response.Token;
            Config.customerId = response.CustomerId;
            return response;
          } else {
            let errorMessage = this.getErrorMessage(response.ErrorList);
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }

  logout() {
    Config.token = "";
  }

  register(model: RegisterModel): Observable<string> {

    const url = Config.apiUrl + 'api/customer/register';
    return this.http
      .post<ServerResponse>(url, model)
      .pipe(
        map((response) => {
          if (response.StatusCode == 200) {
            return response.SuccessMessage;
          } else {
            let errorMessage = this.getErrorMessage(response.ErrorList);
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }
}
