import { HttpErrorResponse } from "@angular/common/http";
import { throwError } from "rxjs";

export class ServiceBase {
    protected errorHandler(error: HttpErrorResponse) {
        console.log(error);
        return throwError(error.message || "Server Error");
    }

    protected getErrorMessage(errorList: Array<string>): string {
        let errorMessage = "";
        errorList.forEach(error => {
            if (errorMessage.length == 0) {
                errorMessage = error;
            } else {
                errorMessage += `\n${error}`;
            }
        });
        return errorMessage;
    }
}