import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { OrderInformation } from '../models/checkout/OrderInformation';
import { Config } from '../configurations/Config';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { ServiceBase } from './base/service-base.service';
import { OrderDto } from '../models/checkout/dtos/OrderDto';
import { AddressDto } from '../models/checkout/dtos/AddressDto';
import { OrderItemDto } from '../models/checkout/dtos/OrderItemDto';
import { OrderCustomerDto } from '../models/checkout/dtos/OrderCustomerDto';

@Injectable()
export class CheckoutService extends ServiceBase {

  constructor(
    private http: HttpClient
  ) {
    super();
  }

  getOrderInformation(): Observable<OrderInformation> {
    const url = Config.apiUrl + 'api/shoppingcart/checkoutorderinformation';

    return this.http
      .get<OrderInformation>(url)
      .pipe(
        map((response) => {
          if (response.StatusCode === 200) {
            return response;
          } else {
            let errorMessage = this.getErrorMessage(response.ErrorList);
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }

  saveOrder(orderInformation: OrderInformation): Observable<any> {
    const url = Config.apiUrl + 'api/orders';
    // Create order
    let order = this.CreateOrderDto(orderInformation);

    console.log(order.shipping_method);
    let postObject = { order: order };
    return this.http
      .post(url, postObject)
      .pipe(
        map((response) => {
          return response;
        }),
        catchError(this.errorHandler)
      );
  }

  getCustomer(customerId: number): Observable<OrderCustomerDto> {
    const customerUrl = Config.apiUrl + 'api/customers/' + customerId;
    return this.http.get(customerUrl)
      .pipe(
        map((response) => {
          return response["customers"][0];
        }),
        catchError(this.errorHandler)
      );
  }


  private CreateOrderDto(orderInformation: OrderInformation): OrderDto {

    let orderReviewData = orderInformation.ShoppingCartModel.OrderReviewData;
    let billingAddress = orderInformation.ShoppingCartModel.OrderReviewData.BillingAddress;
    let shippingAddres = orderInformation.ShoppingCartModel.OrderReviewData.ShippingAddress;

    let order = new OrderDto(
      orderReviewData.SelectedPickUpInStore,
      orderReviewData.ShippingMethod,
      Config.customerId);

    order.billing_address = new AddressDto(
      billingAddress.Id,
      billingAddress.FirstName,
      billingAddress.LastName,
      billingAddress.Email,
      billingAddress.PhoneNumber,
      billingAddress.Company,
      billingAddress.CountryId,
      billingAddress.CountryName,
      billingAddress.StateProvinceId,
      billingAddress.City,
      billingAddress.Address1,
      billingAddress.Address2,
      billingAddress.ZipPostalCode,
      billingAddress.FaxNumber,
      billingAddress.StateProvinceName);

    order.shipping_address = new AddressDto(
      shippingAddres.Id,
      billingAddress.FirstName,
      billingAddress.LastName,
      billingAddress.Email,
      billingAddress.PhoneNumber,
      billingAddress.Company,
      billingAddress.CountryId,
      billingAddress.CountryName,
      billingAddress.StateProvinceId,
      billingAddress.City,
      billingAddress.Address1,
      billingAddress.Address2,
      billingAddress.ZipPostalCode,
      billingAddress.FaxNumber,
      billingAddress.StateProvinceName);

    order.order_items = null;
    return order;
  }

}
