import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, forkJoin } from 'rxjs';
import { Product } from '../models/catalog/Product';
import { Config } from '../configurations/Config';
import { map, catchError } from 'rxjs/operators';
import { Category } from '../models/catalog/Category';
import { ServiceBase } from './base/service-base.service';
import { CategoriesResponse } from '../models/catalog/server-response/CategoriesResponse';
import { ProductsResponse } from '../models/catalog/server-response/ProductsResponse';
import { ProductResponse } from '../models/catalog/server-response/ProductResponse';
import { CategoryResponse } from '../models/catalog/server-response/CategoryResponse';

@Injectable()
export class CatalogService extends ServiceBase {

  constructor(private http: HttpClient) {
    super();
  }

  // Categories
  getFeaturedCategories(): Observable<Category[]> {
    const url = Config.apiUrl + "api/homepagecategories";

    return this.http
      .get<CategoriesResponse>(url)
      .pipe(
        map(response => {
          if (response.StatusCode === 200) {
            return response.Data;
          } else {
            let errorMessage = this.getErrorMessage(response.ErrorList);
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }

  getCategories(): Observable<Category[]> {
    var url = Config.apiUrl + "api/v1/categories";
    return this.http
      .get<CategoriesResponse>(url)
      .pipe(
        map(response => {
          if (response.StatusCode === 200) {
            return response.Data;
          } else {
            let errorMessage = this.getErrorMessage(response.ErrorList);
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }

  // Products
  getFeaturedProducts(): Observable<Product[]> {
    const url = Config.apiUrl + "api/homepageproducts?320";

    return this.http
      .get<ProductsResponse>(url)
      .pipe(
        map(response => {
          if (response.StatusCode === 200) {
            return response.Data;
          } else {
            let errorMessage = this.getErrorMessage(response.ErrorList);
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }

  getCategoryWithProducts(categoryId: number): Observable<Category> {
    const url = Config.apiUrl + "api/category/" + categoryId;

    return this.http
      .get<CategoryResponse>(url)
      .pipe(
        map((response) => {
          if (response.StatusCode === 200) {
            let category = new Category();
            category.Name = response.Name;
            category.Products = response.Products;
            return category;
          }
          else {
            let errorMesasge = this.getErrorMessage(response.ErrorList);
            throw new Error(errorMesasge);
          }
        }),
        catchError(this.errorHandler)
      );

  }

  getProductById(productId: number): Observable<Product> {
    const url = Config.apiUrl + "api/productdetails/" + productId;

    return this.http
      .get<ProductResponse>(url)
      .pipe(
        map(response => {
          if (response.StatusCode === 200) {
            return response.Data;
          } else {
            let errorMessage = this.getErrorMessage(response.ErrorList);
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }

}
