import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Config } from '../configurations/Config';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ServiceBase } from './base/service-base.service';
import { AddToCartResponse } from '../models/cart/server-response/AddToCartResponse';
import { ShoppingCart } from '../models/cart/ShoppingCart';
import { ShoppingCartItem } from '../models/cart/ShoppingCartItem';

@Injectable()
export class CartService extends ServiceBase {

  constructor(
    private http: HttpClient
  ) {
    super();
  }

  addToCart(productId: number, quantity: number): Observable<number> {
    const url = Config.apiUrl + `api/AddProductToCart/${productId}/1`
    const body = [
      { "value": quantity, "key": "addtocart_" + productId + ".EnteredQuantity" }
    ];

    return this.http
      .post<AddToCartResponse>(url, body)
      .pipe(
        map((response) => {
          if (response.StatusCode === 200) {
            return response.Count;
          } else {
            let errorMessage = this.getErrorMessage(response.ErrorList);
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      )
  }

  updateCartItem(cartItem: ShoppingCartItem, quantity: number): Observable<number> {
    const url = Config.apiUrl + `api/AddProductToCart/${cartItem.ProductId}/1`
    const body = [
      { "value": quantity, "key": "addtocart_" + cartItem.ProductId + ".EnteredQuantity" },
      { "value": cartItem.Id, "key": "addtocart_" + cartItem.ProductId + ".UpdatedShoppingCartItemId" }
    ];

    return this.http
      .post<AddToCartResponse>(url, body)
      .pipe(
        map((response) => {
          if (response.StatusCode === 200) {
            return response.Count;
          } else {
            let errorMessage = this.getErrorMessage(response.ErrorList);
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      )
  }

  removeCartItem(cartItemId: number): Observable<ShoppingCart> {
    const url = Config.apiUrl + "api/ShoppingCart/UpdateCart"
    const body = [
      { "value": 0, "key": "itemquantity1" },
      { "value": cartItemId, "key": "removefromcart" }
    ];

    return this.http
      .post<ShoppingCart>(url, body)
      .pipe(
        map((response) => {
          if (response.StatusCode === 200) {
            return response;
          } else {
            let errorMessage = this.getErrorMessage(response.ErrorList);
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      )
  }

  getShoppingCart(): Observable<ShoppingCart> {
    const url = Config.apiUrl + "api/ShoppingCart";

    return this.http
      .get<ShoppingCart>(url)
      .pipe(
        map((response) => {
          if (response.StatusCode === 200) {
            return response;
          } else {
            let errorMessage = this.getErrorMessage(response.ErrorList);
            throw new Error(errorMessage);
          }
        }),
        catchError(this.errorHandler)
      );
  }
}
