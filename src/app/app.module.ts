import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { AppComponent } from "./components/app/app.component";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { JWTInterceptor } from "./interceptors/jwt.interceptor";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { AppRoutingModule } from "./app-routing.module";
import { CatalogService } from "./services/catalog.service";
import { PortraitCardComponent } from './components/portrait-card/portrait-card.component';
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { SectionListViewComponent } from './components/section-list-view/section-list-view.component';
import { ProductsComponent } from './components/products/products.component';
import { HomeComponent } from "./components/home/home.component";
import { ProductComponent } from './components/product/product.component';
import { AppActionBarComponent } from './components/app-action-bar/app-action-bar.component';
import { AuthenticationService } from "./services/authentication.service";
import { CartService } from "./services/cart.service";
import { LoginComponent } from "./components/authentication/login/login.component";
import { RegisterComponent } from "./components/authentication/register/register.component";
import { CartComponent } from './components/cart/cart.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { CheckoutService } from "./services/checkout.service";
import { DropDownModule } from 'nativescript-drop-down/angular'
import { CarouselDirective } from 'nativescript-ng2-carousel/nativescript-ng2-carousel'

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        AppRoutingModule,
        NativeScriptModule,
        NativeScriptFormsModule,
        NativeScriptCommonModule,
        NativeScriptUISideDrawerModule,
        NativeScriptHttpClientModule,
        NativeScriptUIListViewModule,
        DropDownModule
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        PortraitCardComponent,
        ProductsComponent,
        SectionListViewComponent,
        ProductComponent,
        AppActionBarComponent,
        LoginComponent,
        RegisterComponent,
        CartComponent,
        CheckoutComponent,
        CarouselDirective
    ],
    providers: [
        AuthenticationService,
        CartService,
        CatalogService,
        CheckoutService,
        { provide: HTTP_INTERCEPTORS, useClass: JWTInterceptor, multi: true },
        // { provide: HTTP_INTERCEPTORS, useClass: JWTInterceptor, multi: true }

    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
