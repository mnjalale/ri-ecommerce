export class ProductPrice {
    public CurrencyCode: string;
    public Price: string;
    public PriceWithDiscount: string;
    public PriceValue: number;
    public PriceWithDiscountValue: number;
    public ProductId: number;
}