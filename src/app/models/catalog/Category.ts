import { PictureModel } from "./PictureModel";
import { Product } from "./Product";

export class Category {
    constructor() {
        this.Children = new Array<Category>();
        this.DefaultPictureModel = new PictureModel();
        this.Products = new Array<Product>();
    }

    public Id: number;
    public Name: string;
    public ProductCount: number;
    public ParentCategoryId: number;
    public DisplayOrder: number;
    public IconPath: string;
    public Extension: string;
    public Children: Array<Category>;
    public DefaultPictureModel: PictureModel;
    public Products: Array<Product>;

}