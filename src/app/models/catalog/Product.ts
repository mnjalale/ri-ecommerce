import { ProductPrice } from "./ProductPrice";
import { PictureModel } from "./PictureModel";

export class Product {
    constructor() {
        this.ProductPrice = new ProductPrice();
        this.DefaultPictureModel = new PictureModel();
    }
    public Id: number;
    public Name: string;
    public Sku: string;
    public ShortDescription: string;
    public FullDescription: string;
    public ImageUrl: string;
    public ProductPrice: ProductPrice;
    public DefaultPictureModel: PictureModel;

}