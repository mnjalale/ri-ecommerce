export class PictureModel {
    public ImageUrl: string;
    public Title: string;
    public AlternateText: string;
}