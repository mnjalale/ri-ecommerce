import { ServerResponse } from "../../utils/ServerResponse";
import { Product } from "../Product";

export class ProductResponse extends ServerResponse {
    public Data: Product;
}