import { ServerResponse } from "../../utils/ServerResponse";
import { Product } from "../Product";

export class ProductsResponse extends ServerResponse {
    public Data: Array<Product>;
}