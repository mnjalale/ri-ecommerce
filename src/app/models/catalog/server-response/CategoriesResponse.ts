import { ServerResponse } from "../../utils/ServerResponse";
import { Category } from "../Category";

export class CategoriesResponse extends ServerResponse {
    public Data: Array<Category>;
}