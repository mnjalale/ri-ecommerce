import { ServerResponse } from "../../utils/ServerResponse";
import { Product } from "../Product";

export class CategoryResponse extends ServerResponse {
    public Name: string;
    public Products: Array<Product>;
}