export class ServerResponse {
    public SuccessMessage: string;
    public StatusCode: number;
    public ErrorList: Array<string>;
}