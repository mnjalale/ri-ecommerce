import { ServerResponse } from "../utils/ServerResponse";

export class LoginResult extends ServerResponse {
    public FirstName: string;
    public LastName: string
    public Email: string;
    public Username: string;
    public StreetAddress: string;
    public StreetAddress2: string;
    public City: string;
    public Phone: string;
    public CountryId: number;
    public StateProvinceId: number
    public CustomerId: number
    public Token: string;
}