export class RegisterModel {
    public FirstName: string;
    public LastName: string;
    public Email: string;
    public Phone: string;
    public Password: string;
    public ConfirmPassword: string;
}