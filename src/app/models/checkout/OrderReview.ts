import { Address } from "./Address";

export class OrderReview {
    constructor() {
        this.BillingAddress = new Address();
        this.ShippingAddress = new Address();
        this.PickupAddress = new Address();
    }
    public Display: boolean;
    public BillingAddress: Address;
    public IsShippable: boolean;
    public ShippingAddress: Address;
    public PickupAddress: Address;
    SelectedPickUpInStore: boolean;
    ShippingMethod: string;
    PaymentMethod: string;
}