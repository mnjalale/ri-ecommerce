import { ServerResponse } from "../utils/ServerResponse";
import { OrderTotal } from "../cart/OrderTotal";
import { ShoppingCart } from "../cart/ShoppingCart";

export class OrderInformation extends ServerResponse {

    constructor() {
        super();
        this.OrderTotalModel = new OrderTotal();
        this
    }

    public OrderTotalModel: OrderTotal;
    public ShoppingCartModel: ShoppingCart;
}