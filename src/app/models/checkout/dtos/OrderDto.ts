import { OrderItemDto } from "./OrderItemDto";
import { AddressDto } from "./AddressDto";
import { OrderCustomerDto } from "./OrderCustomerDto";

export class OrderDto {
    constructor(
        public pick_up_in_store: boolean,
        public shipping_method: string,
        public customer_id: number
    ) {
        this.order_items = new Array<OrderItemDto>();
        this.customer = new OrderCustomerDto();
        this.shipping_rate_computation_method_system_name = "Pickup.PickupInStore"
        this.payment_method_system_name = "Payments.CashOnDelivery";
    }

    public id: number;
    public store_id: number;
    public customer_currency_code: string;
    public currency_rate: number;
    public customer_tax_display_type_id: number;
    public vat_number: string;
    public order_shipping_incl_tax: number;
    public order_shipping_excl_tax: number;
    public custom_values_xml: string;
    public deleted: boolean;
    public created_on_utc: Date;
    public authorization_transaction_id: string;
    public authorization_transaction_code: string;
    public authorization_transaction_result: string;
    public capture_transaction_id: string;
    public capture_transaction_result: string;
    public subscription_transaction_id: string;
    public paid_date_utc: Date;
    public affiliate_id: number;
    public customer_ip: string;
    public refunded_amount: number;
    public reward_points_were_added: boolean;
    public checkout_attribute_description: string;
    public customer_language_id: number;
    public payment_method_additional_fee_incl_tax: number;
    public payment_method_additional_fee_excl_tax: number;
    public customer_tax_display_type: string;
    public shipping_rate_computation_method_system_name: string;
    public payment_method_system_name: string;
    public order_subtotal_incl_tax: number;
    public order_subtotal_excl_tax: number;
    public order_sub_total_discount_incl_tax: number;
    public order_sub_total_discount_excl_tax: number;
    public tax_rates: string;
    public order_tax: number;
    public order_discount: number;
    public order_total: number;
    public order_status: string;
    public payment_status: string;
    public shipping_status: string;

    public customer: OrderCustomerDto;
    public billing_address: AddressDto;
    public shipping_address: AddressDto;
    public order_items: Array<OrderItemDto>;
}