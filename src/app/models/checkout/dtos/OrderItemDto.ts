
export class OrderItemDto {

    constructor(
        public quantity: number,
        public unit_price_incl_tax: number,
        public unit_price_excl_tax: number,
        public price_incl_tax: number,
        public price_excl_tax: number,
        public discount_amount_incl_tax: number,
        public discount_amount_excl_tax: number,
        public product_id: number,
    ) {

    }

    public id: number;
    public original_product_cost: number;
    public attribute_description: string;
    public download_count: number;
    public isDownload_activated: boolean;
    public license_download_id: number;
    public item_weight: number;
    public rental_start_date_utc: Date;
    public rental_end_date_utc: Date;
}