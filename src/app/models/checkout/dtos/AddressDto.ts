export class AddressDto {
    constructor(
        public id: number,
        public first_name: string,
        public last_name: string,
        public email: string,
        public phone_number: string,
        public company: string,
        public country_id: number,
        public country: string,
        public state_province_id: number,
        public city: string,
        public address1: string,
        public address2: string,
        public zip_postal_code: string,
        public fax_number: string,
        public province: string
    ) {
    }

    public customer_attributes: string;
    public created_on_utc: Date;
}