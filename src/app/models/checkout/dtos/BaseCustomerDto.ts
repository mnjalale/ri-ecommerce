export class BaseCustomerDto {

    constructor() {
        this.role_ids = new Array<number>();
    }

    public id: number;
    public username: string;
    public email: string;
    public first_name: string;
    public last_name: string;
    public language_id: string;
    public date_of_birth: Date;
    public gender: string;
    public admin_comment: string;
    public is_tax_exempt: boolean;
    public has_shopping_cart_items: boolean;
    public active: boolean;
    public deleted: boolean;
    public is_system_account: boolean;
    public system_name: string;
    public last_ip_address: string;
    public created_on_utc: Date;
    public last_login_date_utc: Date;
    public last_activity_date_utc: Date;
    public registered_in_store_id: number;
    public subscribed_to_newsletter: boolean;
    public role_ids: Array<number>;
}