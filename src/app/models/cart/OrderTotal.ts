import { ServerResponse } from "../utils/ServerResponse";
import { TaxRate } from "./TaxRate";

export class OrderTotal extends ServerResponse {
    constructor() {
        super();
        this.TaxRates = new Array<TaxRate>();
    }

    public IsEditable: boolean;
    public SubTotal: string;
    public SubTotalDiscount: string;
    public Shipping: string;
    public RequiresShipping: boolean;
    public SelectedShippingMethod: string;
    public HideShippingTotal: boolean;
    public PaymentMethodAdditionalFee: string;
    public Tax: string;
    public TaxRates: Array<TaxRate>;
    public DisplayTax: boolean;
    public DisplayTaxRates: boolean
    public OrderTotalDiscount: string;
    public OrderTotal: string;
}