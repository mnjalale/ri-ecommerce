import { ShoppingCartItem } from "./ShoppingCartItem";
import { ServerResponse } from "../utils/ServerResponse";
import { OrderTotal } from "./OrderTotal";
import { OrderReview } from "../checkout/OrderReview";

export class ShoppingCart extends ServerResponse {
    constructor() {
        super();
        this.Items = new Array<ShoppingCartItem>();
        this.OrderTotalResponseModel = new OrderTotal();
    }

    public Count: number;
    public Items: Array<ShoppingCartItem>;
    public OrderTotalResponseModel: OrderTotal;
    public OrderReviewData: OrderReview;
}