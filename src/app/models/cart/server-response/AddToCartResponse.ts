import { ServerResponse } from "../../utils/ServerResponse";

export class AddToCartResponse extends ServerResponse {
    public Count: number;
}