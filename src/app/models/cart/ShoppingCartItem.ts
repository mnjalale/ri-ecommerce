import { PictureModel } from "../catalog/PictureModel";

export class ShoppingCartItem {
    public Id: number;
    public ProductId: number;
    public Sku: string;
    public ProductName: string;
    public ProductSeName: string;
    public Picture: PictureModel;
    public UnitPrice: string;
    public SubTotal: string;
    public Discount: string;
    public Quantity: number;
    public AttributeInfo: string;
}