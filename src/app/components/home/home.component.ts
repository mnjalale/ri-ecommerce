import { Component, OnInit } from "@angular/core";
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { Category } from "~/app/models/catalog/Category";
import { CatalogService } from "~/app/services/catalog.service";
import { Product } from "~/app/models/catalog/Product";
import { Router } from "@angular/router";
import { registerElement } from "nativescript-angular/element-registry";
registerElement("Carousel", () => require("nativescript-carousel").Carousel);
registerElement("CarouselItem", () => require("nativescript-carousel").CarouselItem);

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {

    images = [];

    // Properties
    private _featuredCategories: ObservableArray<Category>;
    get featuredCategories(): ObservableArray<Category> {
        return this._featuredCategories;
    }

    private _featuredProducts: ObservableArray<Product>;
    get featuredProducts(): ObservableArray<Product> {
        return this._featuredProducts;
    }

    // Methods
    constructor(
        private catalogService: CatalogService,
        private router: Router) {
        this.images = [
            { title: "", url: 'https://marsyetu.co.ke/images/thumbs/0000013.jpeg' },
            { title: "", url: 'https://marsyetu.co.ke/images/thumbs/0000014.jpeg' },
            { title: "", url: 'https://marsyetu.co.ke/images/thumbs/0000015.jpeg' },
            { title: "", url: 'https://marsyetu.co.ke/images/thumbs/0000016.jpeg' },
            { title: "", url: 'https://marsyetu.co.ke/images/thumbs/0000017.jpeg' }
        ];
    }

    ngOnInit(): void {
        // Load featured products
        this.catalogService
            .getFeaturedCategories()
            .subscribe(
                (categories) => {
                    this._featuredCategories = new ObservableArray(categories);
                },
                (error) => {
                    alert("Errors occurred while loading categories.");
                });

        // Load featured products
        this.catalogService
            .getFeaturedProducts()
            .subscribe(
                (products) => {
                    this._featuredProducts = new ObservableArray(products);
                },
                (error) => {
                    alert("Errors occurred while loading featured products.");
                }
            );
    }

    goToCategoryProducts(categoryId: number) {
        this.router.navigate(['/products', categoryId]);
    }

    goToProductDetails(productId: number) {
        this.router.navigate(['/product', productId]);
    }

}
