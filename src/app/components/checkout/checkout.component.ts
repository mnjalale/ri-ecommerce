import { Component, OnInit } from '@angular/core';
import { OrderInformation } from '~/app/models/checkout/OrderInformation';
import { CheckoutService } from '~/app/services/checkout.service';
import * as Toast from 'nativescript-toast';
import { Router } from '@angular/router';
import { SelectedIndexChangedEventData } from "nativescript-drop-down";
import { OrderCustomerDto } from '~/app/models/checkout/dtos/OrderCustomerDto';

@Component({
  selector: 'ns-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css'],
  moduleId: module.id,
})
export class CheckoutComponent implements OnInit {

  selectedPickupPoint: string = "";
  pickupPoints: Array<string> = ['Diamond Plaza', 'Peponi Road'];
  orderInformation: OrderInformation = new OrderInformation();
  isBusy: boolean = false;

  constructor(
    private checkoutService: CheckoutService,
    private router: Router
  ) { }

  ngOnInit() {
    this.isBusy = true;
    this.checkoutService
      .getOrderInformation()
      .subscribe(
        (orderInformation) => {
          this.isBusy = false;
          this.orderInformation = orderInformation;
        },
        (error) => {
          this.isBusy = false;
          Toast.makeText(error).show();
        }
      );

  }

  public onchange(args: SelectedIndexChangedEventData) {
    this.selectedPickupPoint = this.pickupPoints[args.newIndex];
    Toast.makeText(this.selectedPickupPoint).show();
  }

  confirmOrder() {
    if (this.pickupPoints.indexOf(this.selectedPickupPoint) == -1) {
      Toast.makeText("Please select the pick up point.").show();
      return;
    }

    // Assign Values
    // this.selectedPickupPoint = "Pickup at " + this.selectedPickupPoint;

    // Order Total Model    
    this.orderInformation.OrderTotalModel.SelectedShippingMethod = this.selectedPickupPoint;

    // Order Review Data
    this.orderInformation.ShoppingCartModel.OrderReviewData.SelectedPickUpInStore = true;
    this.orderInformation.ShoppingCartModel.OrderReviewData.ShippingMethod = this.selectedPickupPoint;

    this.isBusy = true;
    this.checkoutService
      .saveOrder(this.orderInformation)
      .subscribe(
        (response) => {
          this.isBusy = false;
          Toast.makeText("Your order has been submitted successfully", "long").show();
          this.router.navigateByUrl('/home');
        },
        (error) => {
          this.isBusy = false;
          Toast.makeText(error, 'long').show();
        }
      )

  }


}
