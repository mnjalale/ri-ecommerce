import { Component, OnInit, ViewChild } from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import { DrawerTransitionBase, RadSideDrawer, SlideInOnTopTransition } from "nativescript-ui-sidedrawer";
import { filter } from "rxjs/operators";
import * as app from "tns-core-modules/application";
import { Category } from "../../models/catalog/Category";
import { CatalogService } from "~/app/services/catalog.service";


@Component({
    moduleId: module.id,
    selector: "ns-app",
    templateUrl: "app.component.html"
})
export class AppComponent implements OnInit {
    private _activatedUrl: string;
    private _sideDrawerTransition: DrawerTransitionBase;
    public categories: Category[];
    constructor(
        private router: Router,
        private routerExtensions: RouterExtensions,
        private catalogService: CatalogService) {
        // Use the component constructor to inject services.
    }

    ngOnInit(): void {
        this._activatedUrl = "/home";
        this._sideDrawerTransition = new SlideInOnTopTransition();

        this.router.events
            .pipe(filter((event: any) => event instanceof NavigationEnd))
            .subscribe((event: NavigationEnd) => this._activatedUrl = event.urlAfterRedirects);

        this.catalogService
            .getCategories()
            .subscribe(
                (categories) => {
                    this.categories = categories;
                },
                (error) => {
                    alert("Errors occurred while loading categories.");
                });
    }

    get sideDrawerTransition(): DrawerTransitionBase {
        return this._sideDrawerTransition;
    }

    isComponentSelected(category: Category): boolean {
        return this._activatedUrl === '/products/' + category.Id;
    }

    onNavItemTap(category: Category): void {
        const categoryId: number = category.Id;

        this.routerExtensions.navigate(['/products', categoryId], {
            transition: {
                name: "fade"
            }
        });

        // this.router.navigateByUrl('/products/' + categoryId);

        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.closeDrawer();
    }
}
