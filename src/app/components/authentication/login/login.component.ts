import { Component, OnInit } from '@angular/core';
import { LoginModel } from '~/app/models/authentication/LoginModel';
import { AuthenticationService } from '~/app/services/authentication.service';
import * as Toast from 'nativescript-toast';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { AppConstants } from '~/app/configurations/AppConstants';

@Component({
  selector: 'ns-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  moduleId: module.id,
})
export class LoginComponent implements OnInit {

  model: LoginModel = new LoginModel();
  isBusy: boolean = false;
  returnUrl: string;

  constructor(
    private authService: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    // temporary
    // this.model.email = "mnjalale@gmail.com";
    // this.model.password = "nairrinairri86";

    // Logout first
    this.authService.logout();

    this.route.queryParamMap.subscribe((queryParam: ParamMap) => {
      this.returnUrl = queryParam.get(AppConstants.returnUrlQueryParameter) || '/home';
    });
  }

  login() {

    if (!this.model.email
      || this.model.email.trim().length == 0
      || !this.model.password
      || this.model.password.trim().length == 0) {
      Toast.makeText("Email and password are required.").show();
      return;
    }

    this.isBusy = true;
    this.authService.login(this.model)
      .subscribe(
        (result) => {
          Toast.makeText("Login Successful").show();
          this.router.navigateByUrl(this.returnUrl);
          this.isBusy = false;
        },
        (error) => {
          this.isBusy = false;
          Toast.makeText(error).show();
        });
  }

  register() {
    this.router.navigateByUrl('/register');
  }

}
