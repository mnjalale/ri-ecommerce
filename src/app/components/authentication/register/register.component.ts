import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '~/app/services/authentication.service';
import { Router } from '@angular/router';
import { RegisterModel } from '~/app/models/authentication/RegisterModel';
import * as Toast from 'nativescript-toast';

@Component({
  selector: 'ns-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  moduleId: module.id,
})
export class RegisterComponent implements OnInit {

  isBusy: boolean = false;
  model: RegisterModel = new RegisterModel();

  constructor(
    private authService: AuthenticationService,
    private router: Router
  ) {
  }

  ngOnInit() {
  }

  register() {

    let validationMessage = this.getValiationMessage();
    if (validationMessage.length > 0) {
      Toast.makeText(validationMessage, 'long').show();
      return;
    }

    this.isBusy = true;
    this.authService.register(this.model)
      .subscribe(
        (message) => {
          this.isBusy = false;
          Toast.makeText(message).show();
          this.router.navigateByUrl('/login');
        },
        (error) => {
          this.isBusy = false;
          Toast.makeText(error).show();
        }
      )
  }

  login() {
    this.router.navigateByUrl('/login');
  }

  private getValiationMessage(): string {
    let validationMessage: string = '';

    if (!this.model.FirstName || this.model.FirstName.trim().length == 0) {
      validationMessage = 'First Name is required.';
    }

    if (!this.model.LastName || this.model.LastName.trim().length == 0) {
      validationMessage = validationMessage.length == 0
        ? 'Last Name is required.'
        : validationMessage + '\nLast Name is required.';
    }

    if (!this.model.Email || this.model.Email.trim().length == 0) {
      validationMessage = validationMessage.length == 0
        ? 'Email is required.'
        : validationMessage + '\nEmail is required.';
    }

    if (!this.model.Phone || this.model.Phone.trim().length == 0) {
      validationMessage = validationMessage.length == 0
        ? 'Phone is required.'
        : validationMessage + '\nPhone is required.';
    }

    if (!this.model.Password || this.model.Password.trim().length == 0) {
      validationMessage = validationMessage.length == 0
        ? 'Password is required.'
        : validationMessage + '\nPassword is required.';
    }

    if (!this.model.ConfirmPassword || this.model.ConfirmPassword.trim().length == 0) {
      validationMessage = validationMessage.length == 0
        ? 'Password confirmation is required.'
        : validationMessage + '\nPassword confirmation is required.';
    }

    if (this.model.Password
      && this.model.Password.trim().length > 0
      && this.model.ConfirmPassword
      && this.model.ConfirmPassword.trim().length > 0
      && this.model.Password != this.model.ConfirmPassword) {
      validationMessage = validationMessage.length == 0
        ? 'Password and Confirm Password must match.'
        : validationMessage + '\nPassword and Confirm Password must match';
    }

    return validationMessage;
  }
}
