import { Component, OnInit, Input } from '@angular/core';
import { ObservableArray } from 'tns-core-modules/data/observable-array/observable-array';

@Component({
  selector: 'ns-section-list-view',
  templateUrl: './section-list-view.component.html',
  styleUrls: ['./section-list-view.component.css'],
  moduleId: module.id,
})
export class SectionListViewComponent implements OnInit {

  @Input() sectionHeader: string;
  @Input() items: ObservableArray<any>;



  constructor() { }

  ngOnInit() {
  }

}
