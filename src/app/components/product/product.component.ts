import { Component, OnInit } from '@angular/core';
import { CatalogService } from '~/app/services/catalog.service';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Product } from '~/app/models/catalog/Product';
import { AuthenticationService } from '~/app/services/authentication.service';
import { AppConstants } from '~/app/configurations/AppConstants';
import * as Toast from 'nativescript-toast';
import { HttpParams } from '@angular/common/http';
import { CartService } from '~/app/services/cart.service';
import { AppActionBarComponent } from '../app-action-bar/app-action-bar.component';

@Component({
  selector: 'ns-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  moduleId: module.id,
})
export class ProductComponent implements OnInit {

  product: Product = new Product();
  productCount: number = 1;
  isBusy: boolean = false;
  private productCountQueryParameter: string = "productCount";

  constructor(
    private catalogService: CatalogService,
    private cartService: CartService,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthenticationService
  ) { }

  ngOnInit() {
    let productId: number;

    this.route.paramMap.subscribe((params: ParamMap) => {
      productId = parseInt(params.get("productId"));

      this.isBusy = true;
      this.catalogService
        .getProductById(productId)
        .subscribe(
          (product) => {
            this.isBusy = false;
            this.product = product;
          },
          (error) => {
            this.isBusy = false;
            Toast.makeText(error).show();
          }
        );
    });

    this.route.queryParamMap.subscribe((params: ParamMap) => {
      let prodCount = params.get(this.productCountQueryParameter);
      if (prodCount) {
        this.productCount = parseInt(prodCount);
      }
    });
  }

  addProductCount() {
    if (this.productCount >= 1) {
      this.productCount += 1;
    } else {
      this.productCount = 1;
    }
  }

  lessProductCount() {
    if (this.productCount > 1) {
      this.productCount -= 1;
    } else {
      this.productCount = 1;
    }
  }

  addToCart() {
    if (!this.authService.isAuthenticated) {
      const returnUrl = `${this.router.url}?${this.productCountQueryParameter}=${this.productCount}`;
      this.router.navigate(['/login'], { queryParams: { [AppConstants.returnUrlQueryParameter]: returnUrl } });
      return;
    } else {
      this.isBusy = true;
      this.cartService
        .addToCart(this.product.Id, this.productCount)
        .subscribe(
          (cartCount) => {
            this.isBusy = false;
            AppActionBarComponent.CartQuantity = cartCount;
            Toast.makeText(this.product.Name + " has been added to cart.").show();
          },
          (error) => {
            this.isBusy = false;
            Toast.makeText(error).show();
          }
        )
    }

  }

}
