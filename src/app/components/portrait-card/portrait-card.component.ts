import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'portrait-card',
  templateUrl: './portrait-card.component.html',
  styleUrls: ['./portrait-card.component.css'],
  moduleId: module.id,
})
export class PortraitCardComponent implements OnInit {
  @Input() imageUrl: string;
  @Input() title: string;
  @Input() subTitle: string;
  @Input() width: number = 200;
  @Input() height: number = 330;

  constructor() { }

  ngOnInit() {
  }

}
