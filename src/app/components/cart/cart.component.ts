import { Component, OnInit } from '@angular/core';
import { ShoppingCart } from '~/app/models/cart/ShoppingCart';
import { CartService } from '~/app/services/cart.service';
import * as Toast from 'nativescript-toast';
import { ShoppingCartItem } from '~/app/models/cart/ShoppingCartItem';
import { AppActionBarComponent } from '../app-action-bar/app-action-bar.component';
import { Router } from '@angular/router';

@Component({
  selector: 'ns-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
  moduleId: module.id,
})
export class CartComponent implements OnInit {

  isBusy = false;
  shoppingCart: ShoppingCart = new ShoppingCart();

  constructor(
    private cartService: CartService,
    private router: Router
  ) { }

  ngOnInit() {
    this.isBusy = true;
    this.cartService
      .getShoppingCart()
      .subscribe(
        (cart) => {
          this.isBusy = false;
          this.shoppingCart = cart;
        },
        (error) => {
          this.isBusy = false;
          Toast.makeText(error).show();
        }
      );
  }

  removeFromCart(cartItemId: number) {
    this.isBusy = true;
    this.cartService.
      removeCartItem(cartItemId)
      .subscribe(
        (shoppingCart) => {
          this.isBusy = false;
          this.shoppingCart = shoppingCart;
          AppActionBarComponent.CartQuantity = shoppingCart.Count;
        },
        (error) => {
          this.isBusy = false;
          Toast.makeText(error).show();
        }
      );
  }

  addItemCount(cartItem: ShoppingCartItem) {
    cartItem.Quantity += 1;
    this.isBusy = true;
    this.cartService.updateCartItem(cartItem, cartItem.Quantity)
      .subscribe(
        (cartCount) => {
          this.isBusy = false;
          AppActionBarComponent.CartQuantity = cartCount;
        },
        (error) => {
          this.isBusy = false;
          Toast.makeText(error).show();
        }
      );
  }

  reduceItemCount(cartItem: ShoppingCartItem) {
    if (cartItem.Quantity === 1) {
      return;
    }

    cartItem.Quantity -= 1;
    this.isBusy = true;
    this.cartService.updateCartItem(cartItem, cartItem.Quantity)
      .subscribe(
        (cartCount) => {
          this.isBusy = false;
          AppActionBarComponent.CartQuantity = cartCount;
        },
        (error) => {
          this.isBusy = false;
          Toast.makeText(error).show();
        }
      );
  }

  goToCheckout() {
    if (!this.shoppingCart.Items || this.shoppingCart.Items.length == 0) {
      Toast.makeText("Your shopping cart is empty. You can't proceed to checkout.");
      return;
    }

    this.router.navigateByUrl('/checkout');

  }
}
