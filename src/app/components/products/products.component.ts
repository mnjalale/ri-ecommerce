import { Component, OnInit } from '@angular/core';
import { CatalogService } from '~/app/services/catalog.service';
import * as app from "tns-core-modules/application";
import { ObservableArray } from 'tns-core-modules/data/observable-array/observable-array';
import { Product } from '~/app/models/catalog/Product';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular/router';
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';
import { AuthenticationService } from '~/app/services/authentication.service';
import { AppConstants } from '~/app/configurations/AppConstants';
import { CartService } from '~/app/services/cart.service';
import { AppActionBarComponent } from '../app-action-bar/app-action-bar.component';
import * as Toast from 'nativescript-toast';

@Component({
  selector: 'ns-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
  moduleId: module.id,
})
export class ProductsComponent implements OnInit {

  categoryName: string;
  isBusy: boolean = false;

  // Properties
  private _products: ObservableArray<Product>;
  get products(): ObservableArray<Product> {
    return this._products;
  }

  constructor(
    private catalogueService: CatalogService,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthenticationService,
    private cartService: CartService) { }

  ngOnInit() {
    this.isBusy = true;
    this.route.paramMap.subscribe((params: ParamMap) => {
      let categoryId: number = parseInt(params.get("categoryId"));

      this.catalogueService
        .getCategoryWithProducts(categoryId)
        .subscribe(
          (category) => {
            this.isBusy = false;
            this.categoryName = category.Name;
            this._products = new ObservableArray(category.Products);
          },
          (error) => {
            this.isBusy = false;
            alert("Errors occurred while loading the products.");
          }
        );
    })
  }

  onDrawerButtonTap(): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.showDrawer();
  }

  goToProductDetails(productId: number) {
    this.router.navigate(['/product', productId]);
  }

  addToCart(product: Product) {
    if (!this.authService.isAuthenticated) {
      const returnUrl = this.router.url;
      this.router.navigate(['/login'], { queryParams: { [AppConstants.returnUrlQueryParameter]: returnUrl } });
      return;
    } else {
      this.isBusy = true;
      this.cartService
        .addToCart(product.Id, 1)
        .subscribe(
          (cartCount) => {
            this.isBusy = false;
            AppActionBarComponent.CartQuantity = cartCount;
            Toast.makeText(product.Name + " has been added to cart.").show();
          },
          (error) => {
            this.isBusy = false;
            Toast.makeText(error).show();
          }
        )
    }

  }
}
