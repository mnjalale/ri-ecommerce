import { Component, OnInit, Input } from '@angular/core';
import { AuthenticationService } from '~/app/services/authentication.service';
import { Router } from '@angular/router';
import { CartService } from '~/app/services/cart.service';
import * as Toast from 'nativescript-toast';
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { RouterExtensions } from 'nativescript-angular/router';

@Component({
  selector: 'app-action-bar',
  templateUrl: './app-action-bar.component.html',
  styleUrls: ['./app-action-bar.component.css'],
  moduleId: module.id,
})
export class AppActionBarComponent implements OnInit {

  @Input() androidIcon: string = "res://arrow_back";
  @Input() iosIcon: string = "res://navigation/arrow_back";
  @Input() title: string = "";

  static CartQuantity: number = 0;

  isAuthenticated: boolean = false;

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private cartService: CartService,
    private routerExtensions: RouterExtensions
  ) { }

  get cartQuantity(): number {
    return AppActionBarComponent.CartQuantity
  }

  ngOnInit() {
    this.isAuthenticated = this.authService.isAuthenticated;

    // Get cart details
    if (this.isAuthenticated) {
      this.cartService
        .getShoppingCart()
        .subscribe(
          (response) => {
            AppActionBarComponent.CartQuantity = response.Count;
          },
          (error) => {
            Toast.makeText(error).show();
          }
        );
    } else {
      AppActionBarComponent.CartQuantity = 0;
    }


  }

  onNavigationButtonTap(): void {
    if (this.androidIcon == "res://arrow_back" || this.iosIcon == "res://navigation/arrow_back") {
      this.routerExtensions.backToPreviousPage();
    } else if (this.androidIcon == "res://menu" || this.iosIcon == "res://navigation/menu") {
      const sideDrawer = <RadSideDrawer>app.getRootView();
      sideDrawer.showDrawer();
    }
  }

  goToLogin() {
    this.router.navigateByUrl('/login');
  }

  goToMyAccount() {

  }

  goToOrders() {

  }

  goToCart() {
    if (AppActionBarComponent.CartQuantity == 0) {
      Toast.makeText("Your cart is empty.").show();
    } else {
      this.router.navigateByUrl('/cart');
    }
  }

  logout() {
    this.authService.logout();
    this.isAuthenticated = this.authService.isAuthenticated;
    this.router.navigateByUrl('/home');
  }

}
