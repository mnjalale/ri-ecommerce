export class AppConstants {
    static returnUrlQueryParameter = "returnUrl";

    static getNumericPrice(formattedPrice: string): number {
        if (!formattedPrice || formattedPrice.trim().length === 0) {
            return null;
        }
        let price = formattedPrice.replace(/[^\d.-]/g, '');
        return parseFloat(price);
    }
}