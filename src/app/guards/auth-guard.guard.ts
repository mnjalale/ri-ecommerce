import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';
import { AppConstants } from '../configurations/AppConstants';

@Injectable()
export class AuthGuardGuard implements CanActivate {

  constructor(
    private router: Router,
    private authService: AuthenticationService
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    if (this.authService.isAuthenticated) {
      return true;
    }

    this.router.navigate(['/login'], { queryParams: { [AppConstants.returnUrlQueryParameter]: state.url } });
    return false;
  }
}
